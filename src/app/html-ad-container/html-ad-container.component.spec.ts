import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlAdContainerComponent } from './html-ad-container.component';

describe('HtmlAdContainerComponent', () => {
  let component: HtmlAdContainerComponent;
  let fixture: ComponentFixture<HtmlAdContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlAdContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlAdContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
