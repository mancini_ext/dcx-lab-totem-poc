import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-html-ad-container',
  templateUrl: './html-ad-container.component.html',
  styleUrls: ['./html-ad-container.component.scss']
})
export class HtmlAdContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  private destroy$ = new Subject();

  @ViewChild('container') adContainer: ElementRef;
  adUrl = '/prod/ads';

  response = '<img width=\"40\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\" />'

  constructor(private readonly httpClient: HttpClient) { }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.httpClient.get(this.adUrl).pipe(takeUntil(this.destroy$)).subscribe((adArray: any) => {
      this.adContainer.nativeElement.innerHTML = adArray[0].payload;
    })

    // this.adContainer.nativeElement.innerHTML = this.response;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
